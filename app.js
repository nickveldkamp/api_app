// API: https://api.adviceslip.com/advice

const url = "https://api.adviceslip.com/advice";
const auth = "563492ad6f91700001000001679c8785858146738820ff099976a361";

const gallery = document.querySelector(".gallery");
const searchInput = document.querySelector(".search-input");
const form = document.querySelector(".search-form");
let searchValue;

searchInput.addEventListener("input", updateInput);
form.addEventListener("submit", (e) => {
  e.preventDefault();
  searchPhotos(searchValue);
});

// async function curatedPhotos() {
//   const dataFetch = await fetch(
//     "https://api.pexels.com/v1/curated?per_page=15",
//     {
//       method: "GET",
//       headers: {
//         Accept: "application/json",
//         Authorization: auth,
//       },
//     }
//   );
//   const data = await dataFetch.json();
//   console.log(data);
//   data.photos.forEach((photo) => {
//     const galleryImg = document.createElement("div");
//     galleryImg.classList.add("gallery-img");
//     galleryImg.innerHTML = `<img src=${photo.src.large}> </img>
//     <p>${photo.photographer}</p>}
//       `;
//     gallery.appendChild(galleryImg);
//   });
// }

function updateInput(e) {
  searchValue = e.target.value;
}

async function searchPhotos(query) {
  removeImages();

  //console.log(searchInput);

  const dataFetch = await fetch(
    `https://api.pexels.com/v1/search?query=${searchValue}&per_page=2`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: auth,
      },
    }
  );
  const data = await dataFetch.json();
  data.photos.forEach((photo) => {
    const galleryImg = document.createElement("div");
    galleryImg.classList.add("gallery-img");
    galleryImg.innerHTML = `<img src=${photo.src.large}> </img>
    <p>${photo.photographer}</p>}
      `;
    gallery.appendChild(galleryImg);
  });
}

function removeImages() {
  let pexelImages = document.querySelectorAll(".gallery-img");

  if (pexelImages) {
    pexelImages.forEach((images) => {
      images.remove();
    });
  }
}

// https://api.pexels.com/v1/search?query=nature&per_page=1
// "https://api.pexels.com/v1/curated?per_page=1";
